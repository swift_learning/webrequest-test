# README #

A simple app showing the use of WKWebView see [Screenshots](Screenshots)

### What is this repository for? ###

* Shows how add a webView to an app
* Shows how to apply the loading of a view in viewDidAppear(not on viewDidLoad as usual) so that constraints(position and size) are correctly applied
* Shows how to allow HTTP requests in an >=9.0 iOS app 

### How do I get set up? ###

* Clone it and open in Xcode
* Configuration - look at Info.plist .. especially at AppTransportSecuritySettings![Screen Shot 2016-11-12 at 12.50.28 PM.png](https://bitbucket.org/repo/kp54M7/images/83855596-Screen%20Shot%202016-11-12%20at%2012.50.28%20PM.png)
* Dependencies - None
* Database configuration 
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Screenshpots ###

![Screenshot1](Screenshots/Simulator Screen Shot Nov 12, 2016, 12.54.07 PM.png)
* Repo owner or admin
* Other community or team contact