//
//  ViewController.swift
//  webrequest-test
//
//  Created by iulian david on 11/12/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    
    var webView: WKWebView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        webView = WKWebView()
        container.addSubview(webView)
       
    }

    //Must be after the view loaded
    //Otherwise the constraints will be applied after
    //we read them
    override func viewDidAppear(_ animated: Bool) {
        
        let frame  = CGRect(x: 0, y: 0, width: container.bounds.width, height: container.bounds.height)
        webView.frame = frame
        
        let urlStr = "https://developer.apple.com/swift/blog/"
        loadRequest(urlStr: urlStr)
       
    }
    
    //the function that loads the Url into wkWebView
    func loadRequest(urlStr: String){
        
        let url = URL(string: urlStr)!
        let request = URLRequest(url: url)
        
        webView.load(request)
    }
    
    @IBAction func swiftLoad(_ sender: Any) {
       let urlStr = "https://developer.apple.com/swift/blog/"
        loadRequest(urlStr: urlStr)
    }

    @IBAction func celbLoad(_ sender: Any) {
        let urlStr = "http://perezhilton.com/"
        loadRequest(urlStr: urlStr)
    }
    
    @IBAction func linuxLoad(_ sender: Any) {
        let urlStr = "https://www.gentoo.org/"
        loadRequest(urlStr: urlStr)
    }
    
}

